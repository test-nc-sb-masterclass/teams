package com.netcracker.teams.data.dtos;

import com.netcracker.teams.data.entities.Team;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import java.text.SimpleDateFormat;

@Getter
@Setter
public class TeamDto {
    Long id;
    String name;
    @Schema(description = "Format: yyyy-MM-dd", example = "2021-08-20")
    String startWhen;
    @Schema(description = "Format: yyyy-MM-dd", example = "2021-08-20")
    String endWhen;
    String uniformColor;
    byte[] teamLogo;
    String comment;
    @Schema(description = "Format: yyyy-MM-dd", example = "2021-08-20")
    String createdWhen;
    Long createdByUserId;

    public static TeamDto toModel(Team team) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        TeamDto dto = new TeamDto();
        dto.setId(team.getId());
        dto.setName(team.getName());
        if (team.getStartWhen() != null) {
            dto.setStartWhen(format.format(team.getStartWhen()));
        }
        if (team.getEndWhen() != null) {
            dto.setEndWhen(format.format(team.getEndWhen()));
        }
        dto.setUniformColor(team.getUniformColor());
        dto.setTeamLogo(team.getTeamLogo());

        if (team.getCreatedWhen() != null) {
            dto.setCreatedWhen(format.format(team.getCreatedWhen()));
        }
        dto.setCreatedByUserId(team.getCreatedByUserId());
        return dto;
    }
}
