package com.netcracker.teams.data.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "teams")
@Getter
@Setter
public class Team {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @Temporal(TemporalType.TIMESTAMP)
    private Date startWhen;

    @Temporal(TemporalType.TIMESTAMP)
    private Date endWhen;

    private String uniformColor;

    @Column(name = "team_logo_image", columnDefinition = "BLOB")
    private byte[] teamLogo;

    @Temporal(TemporalType.TIMESTAMP)
    private Date createdWhen;

    private Long createdByUserId;
}
