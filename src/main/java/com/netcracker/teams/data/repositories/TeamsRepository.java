package com.netcracker.teams.data.repositories;

import com.netcracker.teams.data.entities.Team;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeamsRepository extends JpaRepository<Team, Long> {
}
