package com.netcracker.teams.controllers;

import com.netcracker.teams.data.dtos.TeamDto;
import com.netcracker.teams.data.repositories.TeamsRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TeamsController {

    private final TeamsRepository teamsRepository;

    public TeamsController(TeamsRepository teamsRepository) {
        this.teamsRepository = teamsRepository;
    }

    @GetMapping("/teams")
    public ResponseEntity<List<TeamDto>> getTeams() {
        return ResponseEntity.ok(teamsRepository.findAll().stream().map(TeamDto::toModel).toList());
    }

}
